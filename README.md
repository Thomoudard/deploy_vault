# Deploy a Vault container

## Description

Deploy a Vault container without high availability or redundancy

## Requirements

Create a ```.env``` file in the root directory using ```.env_example``` as template.

## Get start

Just run the command bellow

```sh
docker-compose up -d
```

Go to ```https://<VAULT_ADDR>:<FW_PORT_VAULT>```

## Use-full tips

### Create a self-generated SSL certificate

```sh
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out ssl_cert/cert.pem -keyout ssl_cert/key.pem
``` 

## Issues faced

### Volumes

If I used self-managed volumes for data, vault cannot create unseal and root keys due to ```/vault/data``` created in ```root:root``` instead of ```vault:vault```.

### Ports

I cannot launch vault with the port ```8200``` in the container and I didn't solve this issue. I just go to ```8201```.